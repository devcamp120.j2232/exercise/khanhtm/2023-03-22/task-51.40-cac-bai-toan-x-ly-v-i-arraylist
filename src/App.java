import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Collections;
import java.util.*;

public class App {
    public static void main(String[] args) throws Exception {
        //subtask 1 Tạo mới 1 ArrayList Integer, thêm 10 số(kiểu int) và ghi ra terminal ArrayList vừa tạo. Sắp xếp các phần tử theo thứ tự giá trị tăng dần và ghi lại ra terminal ArrayList vừa được sắp xếp
        ArrayList<Integer> numbers1 = new ArrayList<Integer>();
        numbers1.add(5);
        numbers1.add(9);
        numbers1.add(1);
        numbers1.add(3);
        numbers1.add(7);
        numbers1.add(6);
        numbers1.add(8);
        numbers1.add(2);
        numbers1.add(4);
        numbers1.add(0);
        System.out.println("ArrayList truoc khi sap xep: " + numbers1);
        Collections.sort(numbers1);
        System.out.println("ArrayList sau khi sap xep: " + numbers1);

        //subtask 2 Tạo mới 1 ArrayList Integer, thêm 10 số(kiểu int) và ghi ra terminal ArrayList vừa tạo. Tạo một ArrayList mới chứa các số được lấy từ ArrayList trên có giá trị từ 10 đến 100 và ghi ra terminal ArrayList mới này.
        ArrayList<Integer> numbers2 = new ArrayList<Integer>();
        numbers2.add(5);
        numbers2.add(9);
        numbers2.add(1);
        numbers2.add(3);
        numbers2.add(7);
        numbers2.add(6);
        numbers2.add(8);
        numbers2.add(2);
        numbers2.add(4);
        numbers2.add(0);
        System.out.println("ArrayList ban dau: " + numbers2);
        ArrayList<Integer> filterednumbers2 = new ArrayList<Integer>();
        for (int number : numbers2) {
            if (number >= 10 && number <= 100) {
                filterednumbers2.add(number);
            }
        }
            System.out.println("ArrayList moi: " + filterednumbers2 );

        //subtask 3 Tạo mới 1 ArrayList String, thêm 5 màu sắc (kiểu String) và ghi ra terminal ArrayList vừa tạo. Kiểm tra ArrayList này nếu chứa màu vàng thì in ra terminal chữ “OK” còn ngược lại in ra chữ “KO”
        ArrayList<String> colors3 = new ArrayList<String>();
        colors3.add("Red");
        colors3.add("Blue");
        colors3.add("Green");
        colors3.add("Yellow");
        colors3.add("Purple");

        System.out.println("ArrayList ban dau: " + colors3);

        if (colors3.contains("Yellow")) {
            System.out.println("OK");
        } else {
            System.out.println("KO");
        }

        //subtask 4 Tạo mới 1 ArrayList Integer, thêm 10 số(kiểu int) và ghi ra terminal ArrayList vừa tạo. Ghi ra terminal tổng của các số trong ArrayList
        ArrayList<Integer> numbers4 = new ArrayList<Integer>();
        numbers4.add(5);
        numbers4.add(9);
        numbers4.add(1);
        numbers4.add(3);
        numbers4.add(7);
        numbers4.add(6);
        numbers4.add(8);
        numbers4.add(2);
        numbers4.add(4);
        numbers4.add(0);
        System.out.println("ArrayList ban dau: " + numbers4);
        int sum4 = 0;
        for (int number : numbers4) {
            sum4 += number;
        }
        System.out.println("Tong cua cac so trong ArrayList: " + sum4);

        //subtask5 Tạo mới 1 ArrayList String, thêm 5 màu sắc (kiểu String) và ghi ra terminal ArrayList vừa tạo. Xóa bỏ toàn bộ giá trị của ArrayList và ghi lại ra terminal giá trị mới của ArrayList này.
        ArrayList<String> colors5 = new ArrayList<String>();
        colors5.add("Red");
        colors5.add("Blue");
        colors5.add("Green");
        colors5.add("Yellow");
        colors5.add("Purple");
        System.out.println("ArrayList ban dau: " + colors5);
        colors5.clear();
        System.out.println("ArrayList sau khi xoa: " + colors5);

        //subtask 6 Tạo mới 1 ArrayList String, thêm 10 màu sắc (kiểu String) và ghi ra terminal ArrayList vừa tạo. Hoán đổi ngẫu nhiên vị trí các phần tử của ArrayList và ghi lại ra terminal giá trị mới của ArrayList này.
        ArrayList<String> colors6 = new ArrayList<String>();
        colors6.add("Red");
        colors6.add("Blue");
        colors6.add("Green");
        colors6.add("Yellow");
        colors6.add("Purple");
        colors6.add("Orange");
        colors6.add("Pink");
        colors6.add("Black");
        colors6.add("White");
        colors6.add("Gray");
        System.out.println("ArrayList ban dau: " + colors6);
        Collections.shuffle(colors6);
        System.out.println("ArrayList sau khi hoan doi: " + colors6);

        //subtask 7 Tạo mới 1 ArrayList String, thêm 10 màu sắc (kiểu String) và ghi ra terminal ArrayList vừa tạo. Đảo ngược vị trí các phần tử của ArrayList và ghi lại ra terminal giá trị mới của ArrayList này.
        ArrayList<String> colors7 = new ArrayList<String>();
        colors7.add("Red");
        colors7.add("Orange");
        colors7.add("Yellow");
        colors7.add("Green");
        colors7.add("Blue");
        colors7.add("Indigo");
        colors7.add("Violet");
        colors7.add("Pink");
        colors7.add("Brown");
        colors7.add("Black");
        System.out.println(colors7);
        Collections.reverse(colors7);
        System.out.println(colors7);

        //subtask 8 Tạo mới 1 ArrayList String, thêm 10 màu sắc (kiểu String) và ghi ra terminal ArrayList vừa tạo. Ghi ra terminal một List mới được cắt từ phần tử thứ 3  => 7 của ArrayList trên
        ArrayList<String> colors8 = new ArrayList<String>();
        colors8.add("Red");
        colors8.add("Orange");
        colors8.add("Yellow");
        colors8.add("Green");
        colors8.add("Blue");
        colors8.add("Indigo");
        colors8.add("Violet");
        colors8.add("Pink");
        colors8.add("Brown");
        colors8.add("Black");
        System.out.println(colors8);
        List<String> subList = colors8.subList(2, 7);
        System.out.println(subList);

        //subtask 9 Tạo mới 1 ArrayList String, thêm 10 màu sắc (kiểu String) và ghi ra terminal ArrayList vừa tạo. Hoán đổi vị trí của phần tử thứ 3 với phần tử thứ 7 trong ArrayList và ghi lại ra terminal giá trị mới của ArrayList này.
        ArrayList<String> colors9 = new ArrayList<String>();
        colors9.add("Red");
        colors9.add("Orange");
        colors9.add("Yellow");
        colors9.add("Green");
        colors9.add("Blue");
        colors9.add("Indigo");
        colors9.add("Violet");
        colors9.add("Pink");
        colors9.add("Brown");
        colors9.add("Black");
        System.out.println(colors9);
        Collections.swap(colors9, 2, 6);
        System.out.println(colors9);

        //subtask 10 Tạo mới 2 ArrayList Integer, thêm 3 số (kiểu int) vào mỗi ArrayList, ghi ra terminal 2 ArrayList này. Copy một ArrayList vào ArrayList còn lại và ghi lại ra terminal 2 ArrayList này.
        ArrayList<Integer> list101 = new ArrayList<Integer>();
        list101.add(1);
        list101.add(2);
        list101.add(3);

        ArrayList<Integer> list102 = new ArrayList<Integer>();
        list102.add(4);
        list102.add(5);
        list102.add(6);

        System.out.println(list101);
        System.out.println(list102);

        list102.addAll(list101);

        System.out.println(list101);
        System.out.println(list102);
    }
}
